package com.vitu.springrabbitproducerapi.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pessoa {

    private String nome;
    private Long idade;
    private String cpf;
    private Endereco endereco;
    
}