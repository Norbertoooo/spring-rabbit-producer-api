package com.vitu.springrabbitproducerapi.producer;

import com.vitu.springrabbitproducerapi.domain.Endereco;
import com.vitu.springrabbitproducerapi.domain.Pessoa;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class PessoaProducer {

    private final RabbitTemplate rabbitTemplate;

    @Scheduled(fixedDelay = 10000)
    public void produce() {
        log.info("Enviando pessoa");
        rabbitTemplate.convertAndSend("PessoaExchange", "foo.bar.#", createPessoa());
    }

    public Pessoa createPessoa() {
        return Pessoa.builder()
                .nome("vitu")
                .cpf("21312")
                .idade(12L)
                .endereco(Endereco.builder().numero(12L).rua("rua").build())
                .build();
    }

}