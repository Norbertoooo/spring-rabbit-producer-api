package com.vitu.springrabbitproducerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringRabbitProducerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRabbitProducerApiApplication.class, args);
	}

}
