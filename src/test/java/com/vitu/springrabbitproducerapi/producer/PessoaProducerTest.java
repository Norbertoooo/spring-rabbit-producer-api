package com.vitu.springrabbitproducerapi.producer;

import com.vitu.springrabbitproducerapi.domain.Pessoa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
public class PessoaProducerTest {

    @Mock
    RabbitTemplate rabbitTemplate;

    PessoaProducer pessoaProducer;

    @BeforeEach
    void setUp() {
        pessoaProducer = new PessoaProducer(rabbitTemplate);
    }

    @Test
    void produceTest() {

        pessoaProducer.produce();

        verify(rabbitTemplate, times(1)).convertAndSend(anyString(), anyString(), any(Pessoa.class));
    }

}
